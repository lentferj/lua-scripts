ardour { ["type"] = "EditorAction", name = "Russ_Region Normaliser Mix Prep",
	license     = "MIT",
	author      = "Ardour Team? Modified by Russell Cottier",
	description = [[Normalise all regions on selected tracks]]
}

-- A note from Russ: 
-- This is far from the most efficient way to do this, but without scouring the Ardour LUA manual
-- I have put a script together that will allow you to normalise your regions easily. 
-- It should avoid peaking on tracks and bring RMS level as close to -18 dBFS RMS as possible. 
-- Credit to the Ardour team (I think as it was not fully credited) for the original work on the code.
-- Instructions  - 

function factory () return function ()
--  We are not currently using this, and set_selection below crashes Mixbus 5.2
--	local sl = ArdourUI.SelectionList () -- empty selection list

	-- Get Editor GUI Selection

	local sel = Editor:get_selection ()

	-- Prepare undo
	Session:begin_reversible_command ("Lua Normalize")
	local add_undo = false -- keep track if something has changed

--[[ commented out because we are not actually using this at the moment and set_selection crashes Mixbus 5.2
-- For each selected track/bus..
	for route in sel.tracks:routelist ():iter () do
		-- consider only tracks
		local track = route:to_track ()
		if track:isnil() then
			goto continue
		end

		-- Iterate through all regions of the given track
		for region in track:playlist():region_list():iter() do
		
				-- get RegionView (GUI object to be selected)
				local rv = Editor:regionview_from_region (region)
				-- add it to the list of Objects to be selected
				sl:push_back (rv);
		end
		::continue::
	end
	-- set/replace current selection in the editor
	Editor:set_selection (sl, ArdourUI.SelectionOp.Set);
--]]

	-- Iterate through selected regions
	-- JL: if we actually want to work on tracks and derived regions, we would need to use sl.regions:region_list() here
	for r in sel.regions:regionlist ():iter () do
		-- test if it's an audio region
		local ar = r:to_audioregion ();
		if ar:isnil () then 
			goto next
		end

		local peak = ar:maximum_amplitude (nil);
		local rms  = ar:rms (nil);

		if (peak > 0) then
			print ("Region:", r:name (), "peak:", 20 * math.log (peak) / math.log(10), "dBFS")
			print ("Region:", r:name (), "rms :", 20 * math.log (rms) / math.log(10), "dBFS")
		else
			print ("Region:", r:name (), " is silent")
		end

		-- Normalize region
		if (peak > 0) then
			-- prepare for undo
			r:to_stateful ():clear_changes ()
			-- calculate gain.  
			local f_rms = rms / 10 ^ (.05 * -18) -- -18dBFS/RMS
			local f_peak = peak / 10 ^ (.05 * -1) -- -1dbFS/peak
			-- apply gain
			if (f_rms > f_peak) then
				print ("Region:", r:name (), "RMS  normalized by:", -20 * math.log (f_rms) / math.log(10), "dB")
				ar:set_scale_amplitude (1 / f_rms)
			else 
				print ("Region:", r:name (), "peak normalized by:", -20 * math.log (f_peak) / math.log(10), "dB")
				ar:set_scale_amplitude (1 / f_peak)
			end
			-- Save changes (if any) to undo command
			if not Session:add_stateful_diff_command (r:to_statefuldestructible ()):empty () then
				add_undo = true
			end 
		end

		::next::
	end

	-- Commit the combined undo operation
	if add_undo then
		-- The 'nil' command here means to use all collected diffs
		Session:commit_reversible_command (nil)
	else
		Session:abort_reversible_command ()
	end

end end
